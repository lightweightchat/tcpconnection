#include "HeaderResolver.h"
#include "Messages.pb.h"
#include "gtest/gtest.h"
#include <algorithm>
#include <ctime>
#include <vector>

using namespace ::testing;
using namespace std::placeholders;

class HeaderResolver_F : public Test
{
public:

    void SetUp()
    {
        dmSender = "";
        dmReceiver = "";

        bmSender = "";
        userName = "";

        wasUndefinedMsgCalled = false;
        wasKeepAliveCalled = false;
        
        resolver = new HeaderResolver();

        resolver->setDirectHeaderCallback(std::bind(&HeaderResolver_F::DmCallback, this, _1, _2, _3));
        resolver->setBroadcastHeaderCallback(std::bind(&HeaderResolver_F::BcCallback, this, _1, _2));
        resolver->setLoginHeaderCallback(std::bind(&HeaderResolver_F::loginCallback, this, _1));
        resolver->setUndefinedHeaderCallback(std::bind(&HeaderResolver_F::undefinedMsgCallback, this));
        resolver->setKeepAliveHeaderCallback(std::bind(&HeaderResolver_F::keepAliveCallback, this));
        resolver->setConnectedUsersCallback(std::bind(&HeaderResolver_F::connectedUsersCallback, this, _1));
        resolver->setServerLogCallback(std::bind(&HeaderResolver_F::serverLogCallback, this, _1));
        

    }

    void TearDown() 
    {
        delete resolver;
    }

    void DmCallback(const std::string &sender, const std::string &receiver, const std::vector<uint8_t> &message)
    {
        dmSender = sender;
        dmReceiver = receiver;
        defaultMessage = message;
    }

    void BcCallback(const std::string &sender, const std::vector<uint8_t> &message)
    {
        bmSender = sender;
        defaultMessage = message;
    }

    void loginCallback(const std::string &username)
    {
        userName = username;
    }

    void connectedUsersCallback(const std::string &users)
    {
        connectedUsers = users;
    }

    void serverLogCallback(const std::string &log)
    {
        serverLog = log;
    }

    void undefinedMsgCallback()
    {
        wasUndefinedMsgCalled = true;
    }
    void keepAliveCallback()
    {
        wasKeepAliveCalled = true;
    }

protected:
    std::vector<uint8_t> defaultMessage;

    std::string dmSender;
    std::string dmReceiver;
    std::string bmSender;
    std::string userName;
    std::string connectedUsers;
    std::string serverLog;

    bool wasUndefinedMsgCalled = false;
    bool wasKeepAliveCalled = false;

    HeaderResolver *resolver;
};
/*

TEST_F(HeaderResolver_F, ResolverShouldDecodeDirectMsg)
{

    uint8_t serializeBuffer[256] = {0};
    size_t serializedMsgSize = 256;

    NetworkingMessages::DirectHeader dm;
    dm.set_sender("Sender value");
    dm.set_receiver("Receiver value");
    google::protobuf::Any any;
    any.PackFrom(dm);
    any.SerializeToArray(serializeBuffer, serializedMsgSize);

    std::vector<uint8_t> data(serializeBuffer, serializeBuffer + serializedMsgSize);

    resolver->parseHeader(data, defaultMessage);

    ASSERT_STREQ(dmSender.c_str(), "Sender value");
    ASSERT_STREQ(dmReceiver.c_str(), "Receiver value");

}

TEST_F(HeaderResolver_F, ResolverShouldDecodeBroadCastMsg)
{
    uint8_t serializeBuffer[256] = {};
    size_t serializedMsgSize = 256;

    NetworkingMessages::BroadcastHeader bm;
    bm.set_sender("Sender BM value");
    google::protobuf::Any any;
    any.PackFrom(bm);
    any.SerializeToArray(serializeBuffer, serializedMsgSize);

    std::vector<uint8_t> data(serializeBuffer, serializeBuffer + serializedMsgSize);

    resolver->parseHeader(data, defaultMessage);

    ASSERT_STREQ(bmSender.c_str(), "Sender BM value");
}

TEST_F(HeaderResolver_F, ResolverShouldDecodeLoginMsg)
{
    uint8_t serializeBuffer[256] = {};
    size_t serializedMsgSize = 256;
    NetworkingMessages::LoginHeader lm;
    lm.set_username("New user name");
    google::protobuf::Any any;
    any.PackFrom(lm);
    any.SerializeToArray(serializeBuffer, serializedMsgSize);

    std::vector<uint8_t> data(serializeBuffer, serializeBuffer + serializedMsgSize);
    resolver->parseHeader(data, defaultMessage);

    ASSERT_STREQ(userName.c_str(), "New user name");
}

TEST_F(HeaderResolver_F, ResolverShouldDecodeKeepAliveMsg)
{
    uint8_t serializeBuffer[256] = {};
    size_t serializedMsgSize = 256;
    NetworkingMessages::KeepAliveHeader ka;
    google::protobuf::Any any;
    any.PackFrom(ka);
    any.SerializeToArray(serializeBuffer, serializedMsgSize);

    std::vector<uint8_t> data(serializeBuffer, serializeBuffer + serializedMsgSize);
    resolver->parseHeader(data, defaultMessage);

    ASSERT_TRUE(wasKeepAliveCalled);
}


TEST_F(HeaderResolver_F, Resolver_Should_Decode_ConnectedUsers_Message)
{
    uint8_t serializeBuffer[256] = {};
    size_t serializedMsgSize = 256;
    NetworkingMessages::ConnectedUsersHeader header;
    std::string testMsg = "User1 User2 User3 User4 User5";
    header.set_userlist(testMsg);
    google::protobuf::Any any;
    any.PackFrom(header);
    any.SerializeToArray(serializeBuffer, serializedMsgSize);

    std::vector<uint8_t> data(serializeBuffer, serializeBuffer + serializedMsgSize);
    resolver->parseHeader(data, defaultMessage);

    ASSERT_STREQ(connectedUsers.c_str(), testMsg.c_str());
}

TEST_F(HeaderResolver_F, Resolver_Should_Decode_ServerLogs_Message)
{
    uint8_t serializeBuffer[256] = {};
    size_t serializedMsgSize = 256;
    NetworkingMessages::ServerLogHeader header;
    std::string testMsg = "Server Log test";
    header.set_logs(testMsg);
    google::protobuf::Any any;
    any.PackFrom(header);
    any.SerializeToArray(serializeBuffer, serializedMsgSize);

    std::vector<uint8_t> data(serializeBuffer, serializeBuffer + serializedMsgSize);
    resolver->parseHeader(data, defaultMessage);

    ASSERT_STREQ(serverLog.c_str(), testMsg.c_str());
}



TEST_F(HeaderResolver_F, ResolverShouldDoNothingWhenLoginCallbackIsNotRegistered)
{
    uint8_t serializeBuffer[256] = {};
    size_t serializedMsgSize = 256;
    NetworkingMessages::LoginHeader lm;
    lm.set_username("name");
    google::protobuf::Any any;
    any.PackFrom(lm);
    any.SerializeToArray(serializeBuffer, serializedMsgSize);
    resolver->setLoginHeaderCallback(nullptr);

    std::vector<uint8_t> data(serializeBuffer, serializeBuffer + serializedMsgSize);
    resolver->parseHeader(data, defaultMessage);

    ASSERT_STREQ(userName.c_str(), "");
    ASSERT_FALSE(wasUndefinedMsgCalled);
}

TEST_F(HeaderResolver_F, ResolverShouldDoNothingWhenBMCallbackIsNotRegistered)
{
    uint8_t serializeBuffer[256] = {};
    size_t serializedMsgSize = 256;
    NetworkingMessages::BroadcastHeader bm;
    google::protobuf::Any any;
    any.PackFrom(bm);
    any.SerializeToArray(serializeBuffer, serializedMsgSize);
    resolver->setBroadcastHeaderCallback(nullptr);

    std::vector<uint8_t> data(serializeBuffer, serializeBuffer + serializedMsgSize);
    resolver->parseHeader(data, defaultMessage);
    ASSERT_FALSE(wasUndefinedMsgCalled);
}

TEST_F(HeaderResolver_F, ResolverShouldDoNothingWhenKeepAliveCallbackIsNotRegistered)
{
    uint8_t serializeBuffer[256] = {};
    size_t serializedMsgSize = 256;
    NetworkingMessages::KeepAliveHeader ka;
    google::protobuf::Any any;
    any.PackFrom(ka);
    any.SerializeToArray(serializeBuffer, serializedMsgSize);
    resolver->setKeepAliveHeaderCallback(nullptr);

    std::vector<uint8_t> data(serializeBuffer, serializeBuffer + serializedMsgSize);
    resolver->parseHeader(data, defaultMessage);

    ASSERT_FALSE(wasKeepAliveCalled);
    ASSERT_FALSE(wasUndefinedMsgCalled);
}

TEST_F(HeaderResolver_F, ResolverShouldDoNothingWhenDMCallbackIsNotRegistered)
{
    uint8_t serializeBuffer[256] = {};
    size_t serializedMsgSize = 256;
    NetworkingMessages::DirectHeader dm;
    dm.set_sender("sender");
    google::protobuf::Any any;
    any.PackFrom(dm);
    any.SerializeToArray(serializeBuffer, serializedMsgSize);
    resolver->setDirectHeaderCallback(nullptr);

    std::vector<uint8_t> data(serializeBuffer, serializeBuffer + serializedMsgSize);
    resolver->parseHeader(data, defaultMessage);

    ASSERT_STREQ(dmSender.c_str(), "");
    ASSERT_FALSE(wasKeepAliveCalled);
    ASSERT_FALSE(wasUndefinedMsgCalled);
}

TEST_F(HeaderResolver_F, RandomDataShouldCallundefinedMsgCallback)
{
    std::srand(unsigned(std::time(nullptr)));
    std::vector<uint8_t> v(256);
    std::generate(v.begin(), v.end(), std::rand);

    resolver->parseHeader(v, defaultMessage);

    ASSERT_TRUE(wasUndefinedMsgCalled);
}

*/