#pragma once
#include "NetworkFactory.h"
#include "gmock/gmock.h"

class NetworkFactoryMock : public INetworkFactory
{
public:
    MOCK_METHOD(IAsioContext::ptr, makeContext, (), (override));
    MOCK_METHOD(IAsioSocket::ptr, makeSocket, (IAsioContext::ptr context), (override));
    MOCK_METHOD(IAsioAcceptor::ptr, makeAcceptor, (IAsioContext::ptr context, IP version, int port), (override));
    MOCK_METHOD(ITCPConnection::ptr, makeTCPConnection, (IAsioSocket::ptr socket), (override));
    MOCK_METHOD(IAsioResolver::ptr, makeIpResolver, (IAsioContext::ptr context), (override));

    MOCK_METHOD0(Die, void());
    virtual ~NetworkFactoryMock() { Die(); }
};