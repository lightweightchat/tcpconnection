#pragma once

#include "IAsioContext.h"
#include "gmock/gmock.h"
#include "gtest/gtest.h"

class AsioContextMock : public IAsioContext
{
public:
    MOCK_METHOD(void, run, (), (override));
    MOCK_METHOD(void,  stop, (), (override));
    MOCK_METHOD(void, post, (FunctionHandler), (override));
    MOCK_METHOD(asio::io_context&, getContext, (), (override));

    MOCK_METHOD0(Die, void());
    virtual ~AsioContextMock() { Die(); }
};
