#pragma once
#include "Encryptor.h"
#include "gmock/gmock.h"

class EncryptorMock : public IEncryptor
{
public:
    MOCK_METHOD(bool, encrypt, (std::vector<unsigned char> &data), (override));
    MOCK_METHOD(bool, decrypt, (std::vector<unsigned char> &data), (override));

    MOCK_METHOD0(Die, void());
    virtual ~EncryptorMock() { Die(); }
};
