#pragma once
#include "IAsioSocket.h"
#include <iostream>
#include <memory>
#include <queue>
#include <vector>
#include <stdint.h>
#include <atomic>
using onMessageCallback = std::function<void(uint8_t* data, size_t dataLen)>;
using onErrorCallback = std::function<void(asio::error_code ec)>;

class ITCPConnection 
{
public:
    using ConnectionPointer = std::shared_ptr<ITCPConnection>;
    using ptr = std::shared_ptr<ITCPConnection>;
    
    ITCPConnection() = default;
    virtual ~ITCPConnection() = default;

    virtual void Start(onMessageCallback messageHandler, onErrorCallback errorHandler) = 0;
    virtual void Stop() = 0;

    virtual void Post(const uint8_t* message,size_t msgSize) = 0;
};

namespace _connection{
struct Header_t{
    static const constexpr size_t StartFlag =0xFFAAFFAAFFAAFFAA; 
    struct HeaderData{
        size_t protoMsgSize=0;
        size_t startFlag=StartFlag; //sizet instead of eg. uint32_t for padding
    }__attribute__((packed, aligned(1))) data;
    Header_t()=default;
    explicit Header_t(size_t size):data{size}{}
};
}

class TCPConnection : public ITCPConnection, public std::enable_shared_from_this<TCPConnection>
{
public:
    using ConnectionPointer = std::shared_ptr<TCPConnection>;
    
    TCPConnection(IAsioSocket::ptr socket_);
    virtual ~TCPConnection() = default;

    virtual void Start(onMessageCallback messageHandler, onErrorCallback errorHandler) override;
    virtual void Stop() override;
    virtual void Post(const uint8_t* message,size_t msgSize) override;
    
    std::string getUsername() const;
    //asio::streambuf streamBuf{65536};
protected:
    void asyncRead();
    void readHeaderHandler(asio::error_code ec);
    void readBodyHandler(asio::error_code ec, size_t bytesTranferred);
    void readHeader();
    void readBody(size_t bodySize);
    void asyncWrite();
    void onWrite(asio::error_code ec, size_t bytesTransferred);
    void internalErrorReaction(asio::error_code ec);

    bool isBigEndian()
    {
        return htonl(47) == 47;
    }

private:
    using Header_t = _connection::Header_t;
    Header_t rcvHeader;
    IAsioSocket::ptr socket;
    onMessageCallback messageHandler;
    onErrorCallback errorHandler;
    bool isHeaderState=true;
    std::string username;
    std::queue<std::vector<uint8_t>> outgoingMessages;
    std::vector<uint8_t> BodyHeader;

};