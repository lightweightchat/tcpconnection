#pragma once

#include "AES.h"
#include <cstdint>
#include <memory>
#include <vector>

class IEncryptor
{
public:
    IEncryptor() = default;
    virtual ~IEncryptor() = default;

public:
    virtual bool encrypt(std::vector<unsigned char> &data) = 0;
    virtual bool decrypt(std::vector<unsigned char> &data) = 0;
};

class Encryptor : public IEncryptor
{
    static constexpr size_t encryptorDataLengthBlock = 16;
    const std::vector<unsigned char> defaultKey = {0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xaa, 0xbb, 0xcc, 0xdd, 0xee, 0xff};

public:
    Encryptor(std::shared_ptr<AES> aes, std::vector<unsigned char> key = {});
    virtual ~Encryptor() = default;

public:
    virtual bool encrypt(std::vector<unsigned char> &data) override;
    virtual bool decrypt(std::vector<unsigned char> &data) override;

private:
    void addPadding(std::vector<unsigned char> &data);
    bool isDataAligned(std::vector<unsigned char> &data) const;

private:
    std::shared_ptr<AES> m_aes;
    std::vector<unsigned char> m_key;
};